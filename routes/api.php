<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'bing'], function() {
    Route::get('search', 'SearchController@fetchResult');
    Route::get('suggestion', 'SearchController@fetchSuggestion');
    Route::get('images', 'SearchController@fetchImageResult');
});

