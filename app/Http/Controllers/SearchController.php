<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client as GuzzleHttpClient;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7;
use Config;

class SearchController extends Controller
{
    /**
     * Get the result of the query
     *
     * @param  Request $request  Query String
     * @return object
     */
    public function fetchResult(GuzzleHttpClient $client, Request $request)
    {
        $limit = 10;
        $request->page = ($request->page) ? $request->page : 1;
        $offset = ($limit * $request->page) + 1;

        try {
            $url = Config::get('search.bing.search.url');
            $endpoint = $url . '&q=' . $request->search . '&count=' . $limit . '&offset=' . $offset;
            $bingRequest = $client->request('GET', $endpoint);
            $response = json_decode(
                $bingRequest->getBody()
                ->getContents(),
                true
            );
            return response()->json($response, 200);
        } catch (RequestException $exception) {
            if ($exception->hasResponse()) {
                $error = array(
                    'response' => Psr7\str($exception->getResponse()),
                );
                return response()->json($error, 400);
            }
            return response()->json('Error', 400);
        }
    }

    /**
     * Fetch the suggestion
     *
     * @param  Request $request Query String
     * @return object
     */
    public function fetchSuggestion(GuzzleHttpClient $client, Request $request)
    {
        try {
            $url = Config::get('search.bing.autosuggest.url');
            $endpoint = $url . '&q=' . $request->search;
            $bingRequest = $client->request('GET', $endpoint);
            $response = json_decode(
                $bingRequest->getBody()
                ->getContents(),
                true
            );
            if ($response['suggestionGroups'][0]['searchSuggestions']) {
                return response()->json($response['suggestionGroups'][0]['searchSuggestions'], 200);
            }
            return response()->json('Error', 400);
        } catch (RequestException $exception) {
            if ($exception->hasResponse()) {
                $error = array(
                    'response' => Psr7\str($exception->getResponse()),
                );
                return response()->json($error, 400);
            }
            return response()->json('Error', 400);
        }
    }
}
