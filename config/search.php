<?php
return [
    /*
    |-------------------------------------------------------
    --------------------------------------------------------
    | Third Party Search API
    --------------------------------------------------------
    |-------------------------------------------------------
    | This fileis for storing the credentials for the third
       party such as Bing.
     */

    'bing' => [
        'search'      => [
            'url' => 'https://api.cognitive.microsoft.com/bing/v5.0/search?subscription-key=' . env('BING_SEARCH_KEY'),
        ],
        'autosuggest' => [
            'url' => 'https://api.cognitive.microsoft.com/bing/v5.0/Suggestions?subscription-key=' . env('BING_AUTOSUGGEST_KEY'),
        ],
        'image'       => [
            'url' => 'https://api.cognitive.microsoft.com/bing/v5.0/images/search?count=10&offset=0&subscription-key=' . env('BING_SEARCH_KEY'),
        ]
    ],
];
